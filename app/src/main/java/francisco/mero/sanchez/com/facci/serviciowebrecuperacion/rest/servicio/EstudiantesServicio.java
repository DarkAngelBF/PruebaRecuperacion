package francisco.mero.sanchez.com.facci.serviciowebrecuperacion.rest.servicio;

import java.util.List;

import francisco.mero.sanchez.com.facci.serviciowebrecuperacion.rest.constanst.ApiConstanst;
import francisco.mero.sanchez.com.facci.serviciowebrecuperacion.rest.modelo.Estudiantes;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface EstudiantesServicio {

    @GET(ApiConstanst.ESTUDIANTE)
    Call<Estudiantes> getEstudiante(@Path("id")String id);

    @GET (ApiConstanst.ESTUDIANTES)
    Call<List<Estudiantes>> getEstudiantes();

}
