package francisco.mero.sanchez.com.facci.serviciowebrecuperacion.ui.center;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import francisco.mero.sanchez.com.facci.serviciowebrecuperacion.R;
import francisco.mero.sanchez.com.facci.serviciowebrecuperacion.rest.adapter.EstudiantesAdapter;
import francisco.mero.sanchez.com.facci.serviciowebrecuperacion.rest.modelo.Estudiantes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ArrayList<Estudiantes> listaEstudiante;
    RecyclerView recyclerView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView1);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        MostrarEstudiantes();
    }

    private void MostrarEstudiantes() {
        EstudiantesAdapter estudiantesAdapter = new EstudiantesAdapter();
        Call<List<Estudiantes>> call = estudiantesAdapter.getEstudiantes();
        call.enqueue(new Callback<List<Estudiantes>>() {
            @Override
            public void onResponse(Call<List<Estudiantes>> call, Response<List<Estudiantes>> response) {
                List<Estudiantes> list = response.body();
                for (Estudiantes estudiantes: list){
                    Log.e("ESTUDIANTES: ", estudiantes.getNombres());
                    listaEstudiante.add(estudiantes);
                }
                AdaptadorEstudiantes adaptadorEstudiantes = new AdaptadorEstudiantes(listaEstudiante);
                recyclerView.setAdapter(adaptadorEstudiantes);


            }

            @Override
            public void onFailure(Call<List<Estudiantes>> call, Throwable t) {

            }
        });
}
}