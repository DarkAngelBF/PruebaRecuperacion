package francisco.mero.sanchez.com.facci.serviciowebrecuperacion.ui.center;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import francisco.mero.sanchez.com.facci.serviciowebrecuperacion.R;
import francisco.mero.sanchez.com.facci.serviciowebrecuperacion.rest.modelo.Estudiantes;

public class AdaptadorEstudiantes extends RecyclerView.Adapter<AdaptadorEstudiantes.ViewEstudiante> {


    ArrayList<Estudiantes> listaEstudiante;



    public AdaptadorEstudiantes(ArrayList<Estudiantes>listaEstudiante){
        this.listaEstudiante=listaEstudiante;
    }


    @NonNull
    @Override
    public ViewEstudiante onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewE = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        return new ViewEstudiante(viewE);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewEstudiante holder, final int position) {

        holder.textViewId.setText("ID: "+ listaEstudiante.get(position).getId());
        holder.textViewNombres.setText("NOMBRES: "+ listaEstudiante.get(position).getNombres());
        holder.textViewApellidos.setText("APELLIDOS: "+ listaEstudiante.get(position).getApellidos());
        holder.textViewParcialUno.setText("PARCIAL 1: "+ listaEstudiante.get(position).getParcial_uno());
        holder.textViewParcialDos.setText("PARCIAL 2: "+ listaEstudiante.get(position).getParcial_dos());
        holder.textViewAprueba.setText("APRUEBA: "+ listaEstudiante.get(position).getAprueba());

        Picasso.get().load(listaEstudiante.get(position).getImagen()).into(holder.imageView);

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent intent = new Intent(context, Main2Activity.class);
                intent.putExtra("Id", listaEstudiante.get(position).getId());
                context.startActivity(intent);
            }
        });

        holder.filtrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


    }

    @Override
    public int getItemCount() {
        return listaEstudiante.size();
    }

    public class ViewEstudiante extends RecyclerView.ViewHolder{

        TextView textViewId, textViewNombres, textViewApellidos, textViewParcialUno, textViewParcialDos, textViewAprueba;
        ImageView imageView;
        LinearLayout linearLayout;
        Button filtrar;


        public ViewEstudiante(View itemView) {
            super(itemView);

            textViewId = (TextView)itemView.findViewById(R.id.txtID);
            textViewNombres = (TextView)itemView.findViewById(R.id.txtNombres);
            textViewApellidos = (TextView)itemView.findViewById(R.id.txtApellidos);
            textViewParcialUno = (TextView)itemView.findViewById(R.id.txtParcial1);
            textViewParcialDos = (TextView)itemView.findViewById(R.id.txtParcial2);
            textViewAprueba = (TextView)itemView.findViewById(R.id.txtAprueba);

            linearLayout = (LinearLayout)itemView.findViewById(R.id.linerObject);

            imageView = (ImageView)itemView.findViewById(R.id.imageView);
            filtrar = (Button)itemView.findViewById(R.id.filtrar);
        }
    }
}
