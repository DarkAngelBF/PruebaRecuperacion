package francisco.mero.sanchez.com.facci.serviciowebrecuperacion.rest.adapter;

import java.util.List;

import francisco.mero.sanchez.com.facci.serviciowebrecuperacion.rest.constanst.ApiConstanst;
import francisco.mero.sanchez.com.facci.serviciowebrecuperacion.rest.modelo.Estudiantes;
import francisco.mero.sanchez.com.facci.serviciowebrecuperacion.rest.servicio.EstudiantesServicio;
import retrofit2.Call;

public class EstudiantesAdapter extends BaseAdapter implements EstudiantesServicio {
    private EstudiantesServicio estudiantesServicio;

    public EstudiantesAdapter(){
        super(ApiConstanst.BASE_ESTUDIANTES_URL);
        estudiantesServicio = createService(EstudiantesServicio.class);
    }



    @Override
    public Call<Estudiantes> getEstudiante(String id) {
        return estudiantesServicio.getEstudiante(id);
    }

    @Override
    public Call<List<Estudiantes>> getEstudiantes() {
        return estudiantesServicio.getEstudiantes();
    }
}
