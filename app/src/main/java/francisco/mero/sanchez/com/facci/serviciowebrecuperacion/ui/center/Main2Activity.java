package francisco.mero.sanchez.com.facci.serviciowebrecuperacion.ui.center;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import francisco.mero.sanchez.com.facci.serviciowebrecuperacion.R;
import francisco.mero.sanchez.com.facci.serviciowebrecuperacion.rest.adapter.EstudiantesAdapter;
import francisco.mero.sanchez.com.facci.serviciowebrecuperacion.rest.modelo.Estudiantes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main2Activity extends AppCompatActivity {


    TextView textViewId2, textViewNombres2, textViewApellidos2, textViewParcialUno2, textViewParcialDos2, textViewAprueba2;
    ImageView imageView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        textViewId2 = (TextView)findViewById(R.id.txtID2);
        textViewNombres2 = (TextView)findViewById(R.id.txtNombres2);
        textViewApellidos2 = (TextView)findViewById(R.id.txtApellidos2);
        textViewParcialUno2 = (TextView)findViewById(R.id.txtParcial12);
        textViewParcialDos2 = (TextView)findViewById(R.id.txtParcial22);
        textViewAprueba2 = (TextView)findViewById(R.id.txtAprueba2);
        imageView2 = (ImageView)findViewById(R.id.imageView2);

        String id = getIntent().getStringExtra("Id");
        MostrarEstudiantes(id);


    }

    private void MostrarEstudiantes(String id) {

        EstudiantesAdapter estudiantesAdapter = new EstudiantesAdapter();
        Call<Estudiantes> estudiantesCall = estudiantesAdapter.getEstudiante(id);
        estudiantesCall.enqueue(new Callback<Estudiantes>() {
            @Override
            public void onResponse(Call<Estudiantes> call, Response<Estudiantes> response) {
                Estudiantes estudiantes = response.body();
                textViewId2.setText("ID: " +estudiantes.getId().toString());
                textViewNombres2.setText("NOMBRES: " +estudiantes.getNombres().toString());
                textViewApellidos2.setText("APELLIDOS: " +estudiantes.getNombres().toString());
                textViewParcialUno2.setText("PARCIAL 1: "+estudiantes.getParcial_uno().toString());
                textViewParcialDos2.setText("PARCIAL 2: "+estudiantes.getParcial_dos().toString());
                textViewAprueba2.setText("APRUEBA: " +estudiantes.getAprueba().toString());

                Picasso.get().load(estudiantes.getImagen()).into(imageView2);
            }

            @Override
            public void onFailure(Call<Estudiantes> call, Throwable t) {

            }
        });
    }
}
