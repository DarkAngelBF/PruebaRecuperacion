package francisco.mero.sanchez.com.facci.serviciowebrecuperacion.rest.modelo;

import com.google.gson.annotations.SerializedName;

public class Estudiantes {


    @SerializedName("id")
    private String id;

    @SerializedName("nombres")
    private String nombres;

    @SerializedName("apellidos")
    private String apellidos;

    @SerializedName("parcial_uno")
    private String parcial_uno;

    @SerializedName("parcial_dos")
    private String parcial_dos;

    @SerializedName("aprueba")
    private String aprueba;

    @SerializedName("imagen")
    private String imagen;


    public String getId() {
        return id;
    }

    public String getNombres() {
        return nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getParcial_uno() {
        return parcial_uno;
    }

    public String getParcial_dos() {
        return parcial_dos;
    }

    public String getAprueba() {
        return aprueba;
    }

    public String getImagen() {
        return imagen;
    }
}

